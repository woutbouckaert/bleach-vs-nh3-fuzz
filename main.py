import bleach
import nh3
from pathlib import Path
import time
import codecs


path_list = Path('.').glob('**/*.txt')

fuzz_db = []

for path in path_list:
    if str(path) == "_copyright.txt":
        continue

    with open(".\\" + str(path), 'r', encoding='ISO-8859-1') as f:
        tmp = f.read().splitlines()
        for i in tmp:
            fuzz_db.append(i)


fuzz_db = list(set(fuzz_db))

bleach_output = []
nh3_output = []

print("Testing Bleach...")
t0_bleach = time.time()
for t in fuzz_db:
    bleach_output.append(bleach.clean(t))
t1_bleach = time.time()

print("Testing nh3...")
t0_nh3 = time.time()
for t in fuzz_db:
    nh3_output.append(nh3.clean(t))
t1_nh3 = time.time()


differences = []

for i, (b, n) in enumerate(zip(bleach_output, nh3_output)):
    if not b == n:
        differences.append((fuzz_db[i], b, n))

print("Bleach: " + str(round(t1_bleach - t0_bleach, 2)) + " s")
print("Nh3:    " + str(round(t1_nh3 - t0_nh3, 2)) + " s")

print("Num differences... " + str(len(differences)) + " of " + str(len(
    fuzz_db)) + " tested.")

with open('differences.txt', 'w', encoding="utf-8") as outfile:
    outfile.write('\n'.join(str(i) for i in differences))